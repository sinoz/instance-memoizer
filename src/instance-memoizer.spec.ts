import delay from "delay";
import { second } from "msecs";
import test from "tape-promise/tape.js";
import { InstanceMemoizer } from "./instance-memoizer.js";

test("instance-memoizer", async t => {
    let createdCounter = 0;
    let destroyedCounter = 0;

    class Tester {
        public readonly index = ++createdCounter;
        constructor(public readonly message: string) { }
        public destroy() {
            destroyedCounter++;
        }
    }

    const memoizer = new InstanceMemoizer(
        (message: string) => new Tester(message),
        instance => instance.destroy(),
    );

    const memoized1 = memoizer.acquire("one");
    const memoized2 = memoizer.acquire("two");

    t.equal(memoized1.message, "one");
    t.equal(memoized2.message, "two");
    t.equal(memoized1.index, 1);
    t.equal(memoized2.index, 2);
    t.equal(createdCounter, 2);
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 2);
    t.equal(memoizer.refTotal, 2);

    const memoized3 = memoizer.acquire("one");
    const memoized4 = memoizer.acquire("two");

    t.equal(memoized3.message, "one");
    t.equal(memoized4.message, "two");
    t.equal(memoized3.index, 1);
    t.equal(memoized4.index, 2);
    t.equal(createdCounter, 2);
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 2);
    t.equal(memoizer.refTotal, 4);

    t.deepEqual(memoized1, memoized3);
    t.deepEqual(memoized2, memoized4);

    memoizer.release(memoized3);
    memoizer.release(memoized4);
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 2);
    t.equal(memoizer.refTotal, 2);

    memoizer.release(memoized1);
    memoizer.release(memoized2);
    t.equal(destroyedCounter, 2);
    t.equal(memoizer.instanceTotal, 0);
    t.equal(memoizer.refTotal, 0);

});

test("lingering-instance-memoizer", async t => {
    let createdCounter = 0;
    let destroyedCounter = 0;

    class Tester {
        public readonly index = ++createdCounter;
        public destroy() {
            destroyedCounter++;
        }
    }

    const linger = 1 * second;
    const memoizer = new InstanceMemoizer(
        () => new Tester(),
        instance => instance.destroy(),
    );

    const memoized1 = memoizer.acquire();

    t.equal(createdCounter, 1);
    t.equal(destroyedCounter, 0);

    memoizer.release(memoized1, linger);

    await delay(linger / 2);
    t.equal(destroyedCounter, 0);

    const memoized2 = memoizer.acquire();

    t.equal(createdCounter, 1);
    t.equal(destroyedCounter, 0);

    memoizer.release(memoized2, linger);

    await delay(linger / 2);
    t.equal(destroyedCounter, 0);

    await delay(linger);
    t.equal(destroyedCounter, 1);

    const memoized3 = memoizer.acquire();
    const memoized4 = memoizer.acquire();

    t.equal(createdCounter, 2);
    t.equal(destroyedCounter, 1);

    memoizer.release(memoized3, linger);
    memoizer.release(memoized4, 0);

    await delay(linger / 2);
    t.equal(destroyedCounter, 1);

    await delay(linger);
    t.equal(destroyedCounter, 2);
});

test("lingering-instance-memoizer flush", async t => {
    let createdCounter = 0;
    let destroyedCounter = 0;

    class Tester {
        public readonly index = ++createdCounter;
        public destroy() {
            destroyedCounter++;
        }
    }

    const linger = 1 * second;
    const memoizer = new InstanceMemoizer(
        () => new Tester(),
        instance => instance.destroy(),
    );

    const memoized1 = memoizer.acquire();

    t.equal(createdCounter, 1);
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 1);
    t.equal(memoizer.refTotal, 1);

    const memoized2 = memoizer.acquire();

    t.equal(createdCounter, 1);
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 1);
    t.equal(memoizer.refTotal, 2);

    memoizer.release(memoized1, linger);
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 1);
    t.equal(memoizer.refTotal, 1);

    memoizer.flush();
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 1);
    t.equal(memoizer.refTotal, 1);

    memoizer.release(memoized2, linger);
    t.equal(destroyedCounter, 0);
    t.equal(memoizer.instanceTotal, 1);
    t.equal(memoizer.refTotal, 0);

    memoizer.flush();
    t.equal(destroyedCounter, 1);
    t.equal(memoizer.instanceTotal, 0);
    t.equal(memoizer.refTotal, 0);

});
