import assert from "assert";

/**
 * The instance memoizer can cache an instance of a class based on the arguments with which it
 * is created. The instance will be destroyed when it is no longer being used. Usage is tracked
 * via reference counting.
 */
export class InstanceMemoizer<T, A extends Array<unknown>> {
    private teardownTimes = new Map<string, number>();
    private teardownTimers = new Map<string, any>();

    private readonly instances = new Map<string, T>();
    private readonly keys = new Map<T, string>();
    private readonly refs = new Map<string, number>();

    public instanceTotal = 0;
    public refTotal = 0;

    constructor(
        /**
         * Create an instance from the provided arguments
         * @param args arguments that define this object
         */
        protected readonly createInstance: (...args: A) => T,
        /**
         * This method may call a destructor on the instance that is provided
         * @param instance the instance that needs to be destroyed
         */
        protected readonly destroyInstance = (instance: T) => { return; },
        /**
         * Calculates a key from the arguments use to create an instance, please override this
         * method for better performance !
         * @param args arguments used to create this instance that the key represents
         */
        protected readonly makeKey = (...args: A) => JSON.stringify(args),
    ) {
    }

    protected setupInstance(key: string, ...args: A) {
        assert(!this.hasInstance(key), "instance already setup");

        const instance = this.createInstance(...args);
        this.instanceTotal++;

        this.instances.set(key, instance);
        this.keys.set(instance, key);
    }

    protected teardownInstance(key: string) {
        const instance = this.getInstance(key);

        this.instanceTotal--;
        assert(this.instanceTotal >= 0, "instanceTotal should be a positive number");

        this.destroyInstance(instance);

        this.instances.delete(key);
        this.keys.delete(instance);

        this.teardownTimes.delete(key);
        this.teardownTimers.delete(key);
    }

    protected hasInstance(key: string) {
        return this.instances.has(key);
    }

    protected getInstance(key: string) {
        const instance = this.instances.get(key);
        assert(instance !== undefined, "instance not found");

        return instance;
    }

    protected getKey(instance: T) {
        const key = this.keys.get(instance);
        assert(key !== undefined, "key not found");

        return key;
    }

    protected incRef(key: string) {
        let ref = this.refs.get(key) ?? 0;
        ref++;

        this.refs.set(key, ref);

        this.refTotal++;
    }

    protected decRef(key: string) {
        let ref = this.refs.get(key) ?? 0;
        ref--;
        assert(ref >= 0, "ref should be a positive number");

        if (ref > 0) {
            this.refs.set(key, ref);
        }
        if (ref === 0) {
            this.refs.delete(key);
        }

        this.refTotal--;
        assert(this.refTotal >= 0, "refTotal should be a positive number");
    }

    protected hasRef(key: string) {
        return this.refs.has(key);
    }

    /**
     * teardown all instances that are currently lingering
     */
    public flush() {
        this.teardownTimers.forEach((timeout, key) => {
            clearTimeout(timeout);
            this.teardownInstance(key);
        });
    }

    public acquire(...args: A): T {
        const key = this.makeKey(...args);

        if (!this.hasRef(key)) {
            const teardownTimer = this.teardownTimers.get(key);
            if (teardownTimer) {
                clearTimeout(teardownTimer);
                this.teardownTimers.delete(key);
            }
            else {
                this.setupInstance(key, ...args);
            }
        }
        this.incRef(key);

        const instance = this.getInstance(key);
        return instance;
    }

    public release(instance: T, linger = 0) {
        const key = this.getKey(instance);

        const currentTime = new Date().valueOf();
        let teardownTime = currentTime + linger;

        const teardownTimeLast = this.teardownTimes.get(key) ?? 0;
        if (teardownTimeLast > teardownTime) {
            teardownTime = teardownTimeLast;
        }
        else {
            this.teardownTimes.set(key, teardownTime);
        }

        this.decRef(key);
        if (!this.hasRef(key)) {
            assert(!this.teardownTimers.has(key), "teardown timer already set");

            if (teardownTime > currentTime) {
                const timer = setTimeout(
                    () => this.teardownInstance(key),
                    teardownTime - currentTime,
                );
                this.teardownTimers.set(key, timer);
            }
            else {
                this.teardownInstance(key);
            }

        }
    }
}
