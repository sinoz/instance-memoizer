# `instance-memoizer`
A lingering object cache!

## What is this?
With `instance-memoizer` you can cache object instances. Use the `acquire` method to either create an object, or get an already created object from the cache. Then when you are done with the object, user the `release` method to mark the object for destruction. You may also provide a linger period so the object is not detroyed in this period.

Acquiring an object will increase a counter. This counter is decreased when the object is released. When the counter is at 0 a timer is started. When the timer is done, the object is destroyed. Everytime the acquire method is used with the same arguments, the same object is returned.

## Why do I want this?
Well if you have an object that is expensive to instantiate and you are using many times, you might want to use this. For instance network operations are often expensive in terms of time. Or maybe you are doing a really big computation, that is also a good use case for the `instance-memoizer`.

## I don't likt the name `instance-memoizer`
Yeah I don't like it either. If you can come up with a better name, let me know!

## Example
```ts
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";

const memoizer = new InstanceMemoizer(
    () => Symbol(),
);
const a = memoizer.acquire();
const b = memoizer.acquire();

assert.equal(a, b);

memoizer.release(a)
memoizer.release(b)

const c = memoizer.acquire();
assert.notEqual(b, c);

memoizer.release(c);
```